import { Component, OnInit ,Input } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-booking-history',
  templateUrl: './booking-history.component.html',
  styleUrls: ['./booking-history.component.scss']
})
export class BookingHistoryComponent implements OnInit {

  @Input() bookingTicket:boolean=false
  @Input() paperLessTicket:boolean=false
  showViewTicket:boolean=false;
  constructor(private route:Router) { }

  ngOnInit() {
    this.prepareData()
  }

  monthArr=["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]


prepareData(){
 this.setDate()
 this.paperLessTicket=false
 this.bookingTicket=true;
}
setDate(){
 let date=new Date();
 let todayMonth=date.getMonth()
 let todayDate=date.getDate()
 let todayDateYear=date.getFullYear()
 let oneDayBefore=todayDate-1
 let twoDaysBefore=todayDate-2
//  DEC 9,2023 
 let currentBookingDate=`${this.monthArr[todayMonth]} ${todayDate}, ${todayDateYear}`
 let oneDayBeforeBookingDate=`${this.monthArr[todayMonth]} ${oneDayBefore},${todayDateYear}`
 let twoDaysBeforeBookingDate=`${this.monthArr[todayMonth]} ${twoDaysBefore},${todayDateYear}`
// 

/**
 * 07/07/2023
 */
  let today=`${todayDate}/ ${todayMonth}/ ${todayDateYear} `
  let yesterday=`${oneDayBefore}/ ${todayMonth}/ ${todayDateYear} `
  let yesterdayBefore=`${twoDaysBefore}/ ${todayMonth}/ ${todayDateYear} `
 console.log(currentBookingDate)
 for(let i=0;i<2;i++){
  this.bookingArr[i].bookingDate=currentBookingDate
  this.bookingArr[i]["bookingDateString"]=today
 }
 for(let j=2;j<4;j++){
  this.bookingArr[j].bookingDate=oneDayBeforeBookingDate
  this.bookingArr[j]["bookingDateString"]=yesterday
 }
 for(let k=4;k<5;k++){
  this.bookingArr[k].bookingDate=twoDaysBeforeBookingDate
  this.bookingArr[k]["bookingDateString"]=yesterdayBefore
 }
}

goToView(){
  this.showViewTicket=true
}

  bookingArr=[{
    "journeyType":"RETURN",
    "fare":"20.00",
    "startFrom":"CHINTADARIPET",
    "endIn":"VELACHERY",
    "via":null,
    "adult":1,
    "child":0,
    "bookingDate":null,
  },
  {
    "journeyType":"RETURN",
    "fare":"20.00",
    "startFrom":"THIRUNRAVUR",
    "endIn":"GUINDY",
    "via":"MAS-MPK",
    "adult":1,
    "child":0,
    "bookingDate":"null",
  },
  {
    "journeyType":"RETURN",
    "fare":"20.00",
    "startFrom":"CHINTADARIPET",
    "endIn":"VELACHERY",
    "adult":1,
    "child":0,
    "bookingDate":null,
  },{
    "journeyType":"RETURN",
    "fare":"20.00",
    "startFrom":"CHINTADARIPET",
    "endIn":"VELACHERY",
    "adult":1,
    "child":0,
    "bookingDate":null,
  },{
    "journeyType":"RETURN",
    "fare":"20.00",
    "startFrom":"CHINTADARIPET",
    "endIn":"VELACHERY",
    "adult":1,
    "child":0,
    "bookingDate":null,
  }
]
  bookingObj={
    "journeyType":"RETURN",
    "fare":"20.00",
    "startFrom":"CHINTADARIPET",
    "endIn":"VELACHERY",
    "adult":1,
    "child":0,
    "bookingDate":null,
  }
  bookingObj1={
    "journeyType":"RETURN",
    "fare":"20.00",
    "startFrom":"CHINTADARIPET",
    "endIn":"VELACHERY",
    "adult":1,
    "child":0,
    "bookingDate":null,
  }
  bookingObj2={
    "journeyType":"RETURN",
    "fare":"20.00",
    "startFrom":"CHINTADARIPET",
    "endIn":"VELACHERY",
    "adult":1,
    "child":0,
    "bookingDate":null,
  }
 
  bookingObj4={
    "journeyType":"RETURN",
    "fare":"20.00",
    "startFrom":"CHINTADARIPET",
    "endIn":"VELACHERY",
    "adult":1,
    "child":0,
    "bookingDate":null,
  }
  bookingObj878={
    "journeyType":"RETURN",
    "fare":"20.00",
    "startFrom":"CHINTADARIPET",
    "endIn":"VELACHERY",
    "adult":1,
    "child":0,
    "bookingDate":null,
  }


  login(){
   this.route.navigateByUrl("/login") 
  }
}

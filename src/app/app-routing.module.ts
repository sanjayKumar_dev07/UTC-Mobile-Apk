import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookingHistoryComponent } from './booking-history/booking-history.component';
import { LoginPageComponent } from './login/login-page/login-page.component';




const routes: Routes = [

 

  { path: '', component: BookingHistoryComponent },
  {path:'login',component:LoginPageComponent}
  

  
];

@NgModule({
  
  imports: [ RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }

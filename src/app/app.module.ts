import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BookingHistoryComponent } from './booking-history/booking-history.component';


import { RouterModule } from '@angular/router';
import { LoginModule } from './login/login.module';

@NgModule({
  declarations: [
    AppComponent,
    BookingHistoryComponent,





  ],

  imports: [

    BrowserModule,
    AppRoutingModule,
    RouterModule,
    LoginModule



  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
